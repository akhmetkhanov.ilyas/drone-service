# Getting Started

### Technologies used
* RabbitMQ
* PostgreSQL
* Flyway
* Spring-boot
* Kotlin

### Drone-service Documentation
Pet-project for simulating items delivering lifecycle by drones with event sourcing model.

To start service you must have installed:
* docker
* docker-compose

Start service from root folder by command:
* docker-compose up

For communicating with service use postman and predefined postman collection 
* drone.postman_collection.json

### Steps
* Create drone
* Create medication
* Load drone with medications
* Start drone
* Check state entities by getting events
* Also, there is a job with charge mechanism for drones in IDLE state

### What can be improved
* test coverage
* asynchronous sending event (issue with wrong ordering)
* reactive pageable requests for db

