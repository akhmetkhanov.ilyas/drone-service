FROM openjdk:11.0.4-jre-slim

EXPOSE 8080
RUN mkdir -p /app/
ADD build/libs/drone-service-0.0.1-SNAPSHOT.jar /app/drone-service.jar
ENTRYPOINT ["java", "-jar", "/app/drone-service.jar"]