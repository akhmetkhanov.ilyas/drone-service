create extension if not exists pgcrypto;

create table drone
(
    id              uuid primary key default gen_random_uuid(),
    serial_number    varchar(100) not null unique,
    model           varchar(40)  not null,
    weight_limit     smallint     not null,
    battery_capacity smallint     not null,
    state           varchar(255) not null,
    medications_id   uuid[]
);

create table medication
(
    id        uuid primary key default gen_random_uuid(),
    name      varchar(255) not null,
    weight    smallint     not null,
    code      varchar(255) not null,
    image_path varchar(255)
);

create table event
(
    id         uuid primary key default gen_random_uuid(),
    entity_id   uuid         not null,
    entity_type varchar(255) not null,
    state      varchar(255) not null,
    date       timestamp        default now()
);
