package com.musala.droneservice.exception

class BusinessException(
    override val message: String,
    val errorCode: ErrorCode
) : RuntimeException(message)