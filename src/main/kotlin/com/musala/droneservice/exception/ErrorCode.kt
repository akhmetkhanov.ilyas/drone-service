package com.musala.droneservice.exception

enum class ErrorCode {
    VALIDATION_CONSTRAINT,
    NOT_FOUND
}