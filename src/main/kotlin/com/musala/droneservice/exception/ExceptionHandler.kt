package com.musala.droneservice.exception

import com.musala.droneservice.dto.ErrorDto
import mu.KLogging
import org.springframework.http.HttpStatus.BAD_REQUEST
import org.springframework.http.HttpStatus.NOT_FOUND
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice
import org.springframework.web.bind.support.WebExchangeBindException

@RestControllerAdvice
class ExceptionHandler {
    @ExceptionHandler(BusinessException::class)
    fun businessException(e: BusinessException): ResponseEntity<ErrorDto> {
        logger.error(e.message, e.cause)
        return ResponseEntity(
            ErrorDto(
                message = e.message
            ),
            getStatusCode(e.errorCode)
        )
    }

    @ExceptionHandler(WebExchangeBindException::class)
    fun businessException(e: WebExchangeBindException): ResponseEntity<ErrorDto> {
        logger.error(e.message, e.cause)
        return ResponseEntity(
            ErrorDto(
                message = e.fieldError?.defaultMessage
            ),
            BAD_REQUEST
        )
    }

    private fun getStatusCode(errorCode: ErrorCode) =
        when (errorCode) {
            ErrorCode.VALIDATION_CONSTRAINT -> BAD_REQUEST
            ErrorCode.NOT_FOUND -> NOT_FOUND
        }

    private companion object : KLogging()
}