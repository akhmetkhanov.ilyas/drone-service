package com.musala.droneservice.repository

import com.musala.droneservice.entity.Event
import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import java.util.UUID

interface EventRepository : CoroutineCrudRepository<Event, UUID> {
    @Query("select * from event limit :pageSize offset :page")
    suspend fun findAllByPageRequest(page: Int, pageSize: Int): List<Event>

    suspend fun findAllByEntityId(id: UUID): List<Event>
}