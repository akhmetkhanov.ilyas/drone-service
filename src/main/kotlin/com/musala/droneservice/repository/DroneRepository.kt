package com.musala.droneservice.repository

import com.musala.droneservice.entity.Drone
import com.musala.droneservice.entity.DroneState
import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import java.util.UUID

interface DroneRepository : CoroutineCrudRepository<Drone, UUID> {
    suspend fun findBySerialNumber(serialNumber: String): Drone?

    suspend fun findAllByStateAndBatteryCapacityBefore(state: DroneState, batteryCapacity: Int): List<Drone>
}