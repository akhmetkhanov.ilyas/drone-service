package com.musala.droneservice.repository

import com.musala.droneservice.entity.Medication
import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import java.util.UUID

interface MedicationRepository : CoroutineCrudRepository<Medication, UUID>