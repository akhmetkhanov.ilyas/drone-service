package com.musala.droneservice.conroller

import com.musala.droneservice.conroller.DroneController.Companion.ROOT_PATH
import com.musala.droneservice.dto.LoadDroneRequest
import com.musala.droneservice.dto.RegisterDroneRequest
import com.musala.droneservice.service.DroneService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.UUID
import javax.validation.Valid

@RestController
@RequestMapping(ROOT_PATH)
class DroneController(
    private val droneService: DroneService
) {

    @GetMapping
    suspend fun getDrones() = droneService.getAll()

    @GetMapping(ID_PATH)
    suspend fun getDroneById(@PathVariable id: UUID) = droneService.getDroneById(id)

    @PostMapping
    suspend fun register(@Valid @RequestBody request: RegisterDroneRequest) =
        droneService.createNewDrone(request)

    @PutMapping(LOAD_PATH)
    suspend fun loadMedication(@Valid @RequestBody request: LoadDroneRequest) =
        droneService.setMedicationsToDrone(request)

    @PostMapping("/start")
    suspend fun start(@RequestParam id: UUID) = droneService.startDrone(id)

    companion object {
        const val ROOT_PATH = "/drone"
        const val LOAD_PATH = "/load"
        const val ID_PATH = "/{id}"
    }
}