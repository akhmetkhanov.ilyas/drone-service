package com.musala.droneservice.conroller

import com.musala.droneservice.conroller.MedicationController.Companion.ROOT_PATH
import com.musala.droneservice.dto.CreateMedicationRequest
import com.musala.droneservice.service.MedicationService
import org.springframework.http.codec.multipart.FilePart
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestPart
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono
import java.util.UUID
import javax.validation.Valid

@RestController
@RequestMapping(ROOT_PATH)
class MedicationController(
    private val medicationService: MedicationService
) {

    @PostMapping(consumes = ["multipart/form-data"])
    suspend fun createMedication(
        @Valid @RequestPart("data") request: CreateMedicationRequest,
        @RequestPart("image", required = false) image: Mono<FilePart>
    ) = medicationService.addNewItem(request, image)

    @GetMapping(ID_PATH)
    suspend fun getMedicationById(
        @PathVariable id: UUID
    ) = medicationService.getItemById(id)

    @GetMapping
    suspend fun getMedications() = medicationService.findAll()

    companion object {
        const val ROOT_PATH = "/medication"
        const val ID_PATH = "/{id}"
    }
}