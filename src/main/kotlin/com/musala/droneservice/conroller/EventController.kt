package com.musala.droneservice.conroller

import com.musala.droneservice.conroller.EventController.Companion.ROOT_PATH
import com.musala.droneservice.service.EventService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.UUID

@RestController
@RequestMapping(ROOT_PATH)
class EventController(
    private val eventService: EventService
) {

    @GetMapping
    suspend fun getEvents(
        @RequestParam page: Int,
        @RequestParam pageSize: Int
    ) = eventService.getEvents(page, pageSize)

    @GetMapping(DRONE_PATH)
    suspend fun getEventsByEntityId(
        @RequestParam id: UUID
    ) = eventService.getEventByEntityId(id)

    companion object {
        const val ROOT_PATH = "/events"
        const val DRONE_PATH = "/entity"
    }
}