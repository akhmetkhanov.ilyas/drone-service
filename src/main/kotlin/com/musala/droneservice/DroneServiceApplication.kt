package com.musala.droneservice

import com.musala.droneservice.configuration.QueueProperties
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import org.springframework.scheduling.annotation.EnableScheduling

@SpringBootApplication
@EnableConfigurationProperties(
    QueueProperties::class
)
@EnableScheduling
class DroneServiceApplication


fun main(args: Array<String>) {
    runApplication<DroneServiceApplication>(*args)
}
