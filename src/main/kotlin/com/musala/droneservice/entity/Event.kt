package com.musala.droneservice.entity

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table
import java.sql.Timestamp
import java.util.UUID

@Table
data class Event(
    @field: Id
    val id: UUID? = null,
    val entityId: UUID,
    val entityType: String,
    val state: String,
    val date: Timestamp? = null
)
