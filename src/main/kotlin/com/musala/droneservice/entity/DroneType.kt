package com.musala.droneservice.entity

enum class DroneType {
    LIGHT,
    MIDDLE,
    CRUISER,
    HEAVY
}