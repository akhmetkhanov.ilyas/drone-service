package com.musala.droneservice.entity

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table
import java.util.UUID

@Table
data class Medication(
    @field:Id
    val id: UUID? = null,
    val name: String,
    val weight: Int,
    val code: String,
    val imagePath: String?
)