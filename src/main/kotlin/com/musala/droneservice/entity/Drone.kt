package com.musala.droneservice.entity

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table
import java.util.UUID

@Table
data class Drone(
    @field: Id
    val id: UUID? = null,
    val serialNumber: String,
    val model: DroneType,
    val weightLimit: Int,
    var batteryCapacity: Int,
    var state: DroneState,
    var medicationsId: List<UUID>? = null
)
