package com.musala.droneservice.entity

import com.musala.droneservice.dto.State

enum class DroneState : State {
    IDLE,
    LOADING,
    LOADED,
    DELIVERING,
    DELIVERED,
    RETURNING
}