package com.musala.droneservice.configuration

import com.rabbitmq.client.Connection
import org.springframework.amqp.core.AmqpAdmin
import org.springframework.amqp.core.Queue
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.rabbitmq.AcknowledgableDelivery
import reactor.rabbitmq.ConsumeOptions
import reactor.rabbitmq.ExceptionHandlers
import reactor.rabbitmq.ExceptionHandlers.CONNECTION_RECOVERY_PREDICATE
import reactor.rabbitmq.RabbitFlux
import reactor.rabbitmq.ReceiverOptions
import reactor.util.retry.RetrySpec
import java.time.Duration

@Configuration
class AmqpDeliveryConfiguration(
    private val amqpAdmin: AmqpAdmin,
    private val queueProperties: QueueProperties
) {
    @Bean
    fun eventDelivery(droneEventConnection: Mono<Connection>): Flux<AcknowledgableDelivery> =
        createDelivery(droneEventConnection, queueProperties.eventQueue)

    @Bean
    fun processorCommandDelivery(processorCommandConnection: Mono<Connection>): Flux<AcknowledgableDelivery> =
        createDelivery(processorCommandConnection, queueProperties.processorCommandQueue)

    private fun createDelivery(
        connection: Mono<Connection>,
        queueName: String
    ): Flux<AcknowledgableDelivery> {
        amqpAdmin.declareQueue(Queue(queueName))

        val exceptionHandler = ExceptionHandlers.RetryAcknowledgmentExceptionHandler(
            Duration.ofSeconds(5), Duration.ofMillis(500),
            CONNECTION_RECOVERY_PREDICATE
        )

        val options = ConsumeOptions()
            .exceptionHandler(exceptionHandler)

        return createReceiver(connection).consumeManualAck(queueName, options)
    }

    private fun createReceiver(connection: Mono<Connection>) =
        RabbitFlux.createReceiver(
            ReceiverOptions()
                .connectionMonoConfigurator { it.retryWhen(RetrySpec.backoff(3, Duration.ofSeconds(5))) }
                .connectionMono(connection)
        )
}
