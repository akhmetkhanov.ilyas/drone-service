package com.musala.droneservice.configuration

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import javax.validation.constraints.NotBlank

@ConstructorBinding
@ConfigurationProperties(prefix = "queues")
data class QueueProperties(
    @field:NotBlank
    val eventQueue: String,
    @field:NotBlank
    val processorCommandQueue: String
)
