package com.musala.droneservice.configuration

import com.rabbitmq.client.ConnectionFactory
import org.springframework.boot.autoconfigure.amqp.RabbitProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import reactor.core.publisher.Mono


@Configuration
class AmqpConnectionConfiguration(
    private val rabbitProperties: RabbitProperties,
    private val queueProperties: QueueProperties
) {
    @Bean
    fun reactiveAmqpConnectionFactory() = ConnectionFactory()
        .apply {
            host = rabbitProperties.host
            port = rabbitProperties.port
            username = rabbitProperties.username
            password = rabbitProperties.password
        }

    @Bean
    fun droneEventConnection(connectionFactory: ConnectionFactory) =
        getConnection(connectionFactory, queueProperties.eventQueue)

    @Bean
    fun processorCommandConnection(connectionFactory: ConnectionFactory) =
        getConnection(connectionFactory, queueProperties.processorCommandQueue)

    private fun getConnection(connectionFactory: ConnectionFactory, queueName: String) = Mono.fromCallable {
        connectionFactory.newConnection(queueName)
    }.cache()
}
