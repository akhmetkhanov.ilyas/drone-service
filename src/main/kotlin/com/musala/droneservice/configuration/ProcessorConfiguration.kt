package com.musala.droneservice.configuration

import com.musala.droneservice.service.processor.BaseProcessor
import com.musala.droneservice.service.processor.DeliveryProcessor
import com.musala.droneservice.service.processor.LoadProcessor
import com.musala.droneservice.service.processor.ReturnProcessor
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary

@Configuration
class ProcessorConfiguration(
    private val loadProcessor: LoadProcessor,
    private val deliveryProcessor: DeliveryProcessor,
    private val returnProcessor: ReturnProcessor
) {

    @Bean
    @Primary
    fun mainProcessor(): BaseProcessor {
        val firstProcessor = loadProcessor
        listOf(
            firstProcessor,
            deliveryProcessor,
            returnProcessor
        ).reduce { acc, next ->
            acc.setDownStreamProcessor(next)
        }

        return firstProcessor
    }

}