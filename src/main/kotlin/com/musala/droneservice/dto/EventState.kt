package com.musala.droneservice.dto

enum class EventState : State {
    CREATED,
    MEDICATIONS_ADDED,
    MEDICATIONS_REMOVED,
    DELETED
}