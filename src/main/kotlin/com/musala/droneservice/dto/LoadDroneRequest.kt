package com.musala.droneservice.dto

import java.util.UUID
import javax.validation.constraints.NotEmpty

data class LoadDroneRequest(
    val droneId: UUID,
    @field:NotEmpty(message = "medicationIds can't be empty")
    val medicationIds: List<UUID>
)
