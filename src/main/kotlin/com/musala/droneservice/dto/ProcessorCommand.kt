package com.musala.droneservice.dto

import java.util.UUID

data class ProcessorCommand(
    val droneId: UUID
)