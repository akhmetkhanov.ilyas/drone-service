package com.musala.droneservice.dto

import javax.validation.Valid
import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotEmpty

data class CreateMedicationRequest(
    @field:Valid
    val body: Body
)

data class Body(
    @field:NotEmpty(message = "name can't be empty")
    val name: String,
    @field:Max(value = 500, message = "weight must be less than 500 gr")
    @field:Min(value = 1, message = "weight must be more than 0 gr")
    val weight: Int,
    @field:NotEmpty
    val code: String
)