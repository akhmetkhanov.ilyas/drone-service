package com.musala.droneservice.dto

import com.musala.droneservice.entity.DroneType
import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.Size

data class RegisterDroneRequest(
    @field:Size(min = 1, max = 100, message = "serial number length must be from 1 to 100 characters")
    val serialNumber: String,
    val model: DroneType,
    @field:Max(value = 500, message = "weight limit must be less than 500 gr")
    @field:Min(value = 1, message = "weight limit must be more than 0 gr")
    val weightLimit: Int
)
