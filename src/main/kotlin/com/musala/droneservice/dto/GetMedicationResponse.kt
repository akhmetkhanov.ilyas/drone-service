package com.musala.droneservice.dto

import java.util.UUID

data class GetMedicationResponse(
    val id: UUID,
    val name: String,
    val weight: Int,
    val code: String,
    val image: String?
)
