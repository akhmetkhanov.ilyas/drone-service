package com.musala.droneservice.dto

data class ErrorDto(
    val message: String?
)
