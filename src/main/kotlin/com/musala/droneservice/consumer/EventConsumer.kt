package com.musala.droneservice.consumer

import com.musala.droneservice.service.EventService
import org.springframework.stereotype.Component
import reactor.core.publisher.Flux
import reactor.rabbitmq.AcknowledgableDelivery
import javax.annotation.PostConstruct

@Component
class EventConsumer(
    eventDelivery: Flux<AcknowledgableDelivery>,
    private val eventService: EventService
) : AmqpConsumer(
    eventDelivery
) {

    @PostConstruct
    fun run() {
        consumeMessage()
    }

    override suspend fun processMessage(message: AcknowledgableDelivery) {
        eventService.processMessage(message)
    }
}