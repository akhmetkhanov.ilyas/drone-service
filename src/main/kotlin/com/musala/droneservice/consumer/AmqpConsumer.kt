package com.musala.droneservice.consumer

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.reactive.asFlow
import mu.KLogging
import reactor.core.publisher.Flux
import reactor.rabbitmq.AcknowledgableDelivery

abstract class AmqpConsumer(
    private val delivery: Flux<AcknowledgableDelivery>,
) {

    abstract suspend fun processMessage(message: AcknowledgableDelivery)

    open fun consumeMessage() {
        delivery.asFlow()
            .onEach { message ->
                try {
                    processMessage(message)
                    message.ack()
                } catch (e: Throwable) {
                    logger.error("Consumer error, message: $message", e)
                    message.nack(message.envelope.deliveryTag != LIMIT_ATTEMPT)
                }
            }
            .launchIn(CoroutineScope(Dispatchers.Default))
    }

    private companion object : KLogging() {
        const val LIMIT_ATTEMPT = 3L
    }
}