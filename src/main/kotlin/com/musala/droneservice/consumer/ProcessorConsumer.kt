package com.musala.droneservice.consumer

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.musala.droneservice.service.processor.BaseProcessor
import org.springframework.stereotype.Component
import reactor.core.publisher.Flux
import reactor.rabbitmq.AcknowledgableDelivery
import javax.annotation.PostConstruct

@Component
class ProcessorConsumer(
    processorCommandDelivery: Flux<AcknowledgableDelivery>,
    private val mainProcessor: BaseProcessor,
    private val objectMapper: ObjectMapper
) : AmqpConsumer(
    processorCommandDelivery
) {

    @PostConstruct
    fun run() {
        consumeMessage()
    }

    override suspend fun processMessage(message: AcknowledgableDelivery) {
        mainProcessor.process(objectMapper.readValue(message.body))
    }
}