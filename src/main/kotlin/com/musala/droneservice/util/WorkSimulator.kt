package com.musala.droneservice.util

import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlin.random.Random
import kotlin.time.DurationUnit
import kotlin.time.toDuration


fun doSomeWork() = runBlocking {
    delay(
        Random.nextInt(1,10).toDuration(DurationUnit.SECONDS)
    )
}

fun charge() = runBlocking {
    delay(
        Random.nextInt(1,5).toDuration(DurationUnit.SECONDS)
    )
}