package com.musala.droneservice.service.scheduler

import com.musala.droneservice.service.DroneService
import kotlinx.coroutines.runBlocking
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Component
class ChargeScheduler(
    private val droneService: DroneService
) {

    @Scheduled(fixedRate = 10000)
    fun droneCharging() {
        runBlocking {
            droneService.startCharging()
        }
    }

}