package com.musala.droneservice.service

import com.fasterxml.jackson.databind.ObjectMapper
import com.musala.droneservice.configuration.QueueProperties
import com.musala.droneservice.dto.ProcessorCommand
import org.springframework.amqp.core.AmqpTemplate
import org.springframework.stereotype.Service
import java.util.UUID

@Service
class CommandService(
    private val amqpTemplate: AmqpTemplate,
    private val queueProperties: QueueProperties,
    private val objectMapper: ObjectMapper
) {

    suspend fun send(id: UUID) =
        amqpTemplate.convertAndSend(
            queueProperties.processorCommandQueue,
            objectMapper.writeValueAsString(
                ProcessorCommand(
                    droneId = id
                )
            )
        )
}