package com.musala.droneservice.service.processor

import com.musala.droneservice.dto.ProcessorCommand
import com.musala.droneservice.entity.DroneState
import com.musala.droneservice.service.DroneService
import org.springframework.stereotype.Service

@Service
class LoadProcessor(
    private val droneService: DroneService
) : BaseProcessor() {

    override suspend fun process(command: ProcessorCommand) {
        droneService.doWork(
            droneId = command.droneId,
            startState = DroneState.LOADING,
            finishState = DroneState.LOADED,
            reducePercentage = 4
        )

        if (isDownStreamProcessorExist()) {
            nextProcessor.process(command)
        }
    }
}