package com.musala.droneservice.service.processor

import com.musala.droneservice.dto.ProcessorCommand

abstract class BaseProcessor {
    lateinit var nextProcessor: BaseProcessor

    abstract suspend fun process(command: ProcessorCommand)

    fun setDownStreamProcessor(processor: BaseProcessor): BaseProcessor {
        nextProcessor = processor
        return nextProcessor
    }

    fun isDownStreamProcessorExist() = ::nextProcessor.isInitialized
}