package com.musala.droneservice.service.processor

import com.musala.droneservice.dto.ProcessorCommand
import com.musala.droneservice.entity.DroneState
import com.musala.droneservice.service.DroneService
import org.springframework.stereotype.Service

@Service
class DeliveryProcessor(
    private val droneService: DroneService
) : BaseProcessor() {

    override suspend fun process(command: ProcessorCommand) {
        droneService.doWork(
            droneId = command.droneId,
            startState = DroneState.DELIVERING,
            finishState = DroneState.DELIVERED,
            reducePercentage = 12
        )

        if (isDownStreamProcessorExist()) {
            nextProcessor.process(command)
        }
    }
}