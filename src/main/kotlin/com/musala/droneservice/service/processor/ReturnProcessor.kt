package com.musala.droneservice.service.processor

import com.musala.droneservice.dto.ProcessorCommand
import com.musala.droneservice.entity.DroneState
import com.musala.droneservice.service.DroneService
import org.springframework.stereotype.Service

@Service
class ReturnProcessor(
    private val droneService: DroneService
) : BaseProcessor() {

    override suspend fun process(command: ProcessorCommand) {
        droneService.doWork(
            droneId = command.droneId,
            startState = DroneState.RETURNING,
            finishState = DroneState.IDLE,
            reducePercentage = 8
        )

        droneService.removeMedicationsFromDrone(command.droneId)

        if (isDownStreamProcessorExist()) {
            nextProcessor.process(command)
        }
    }
}