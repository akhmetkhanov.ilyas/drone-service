package com.musala.droneservice.service

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.musala.droneservice.configuration.QueueProperties
import com.musala.droneservice.dto.State
import com.musala.droneservice.entity.Drone
import com.musala.droneservice.entity.Event
import com.musala.droneservice.entity.Medication
import com.musala.droneservice.repository.EventRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.springframework.amqp.core.AmqpTemplate
import org.springframework.stereotype.Service
import reactor.rabbitmq.AcknowledgableDelivery
import java.util.UUID

@Service
class EventService(
    private val objectMapper: ObjectMapper,
    private val eventRepository: EventRepository,
    private val amqpTemplate: AmqpTemplate,
    private val queueProperties: QueueProperties
) {

    suspend fun processMessage(message: AcknowledgableDelivery): Unit =
        withContext(Dispatchers.IO) {
            eventRepository.save(objectMapper.readValue(message.body))
        }

    suspend fun send(drone: Drone, state: State) =
        amqpTemplate.convertSendAndReceive(
            queueProperties.eventQueue,
            objectMapper.writeValueAsString(
                Event(
                    entityId = drone.id!!,
                    state = state.toString(),
                    entityType = Drone::class.java.simpleName
                )
            )
        )

    suspend fun send(medication: Medication, state: State) =
        amqpTemplate.convertSendAndReceive(
            queueProperties.eventQueue,
            objectMapper.writeValueAsString(
                Event(
                    entityId = medication.id!!,
                    state = state.toString(),
                    entityType = Medication::class.java.simpleName
                )
            )
        )

    suspend fun getEvents(page: Int, pageSize: Int) =
        eventRepository.findAllByPageRequest(page * pageSize, pageSize)

    suspend fun getEventByEntityId(id: UUID) =
        eventRepository.findAllByEntityId(id)

}