package com.musala.droneservice.service

import com.musala.droneservice.dto.CreateMedicationRequest
import com.musala.droneservice.dto.EventState
import com.musala.droneservice.dto.GetMedicationResponse
import com.musala.droneservice.entity.Medication
import com.musala.droneservice.exception.BusinessException
import com.musala.droneservice.exception.ErrorCode
import com.musala.droneservice.repository.MedicationRepository
import kotlinx.coroutines.reactor.awaitSingle
import kotlinx.coroutines.reactor.awaitSingleOrNull
import org.springframework.core.io.buffer.DataBufferUtils
import org.springframework.http.codec.multipart.FilePart
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import java.io.File
import java.util.Base64
import java.util.UUID
import java.util.regex.Pattern

@Service
class MedicationService(
    private val medicationRepository: MedicationRepository,
    private val eventService: EventService
) {
    private val pattern = Pattern.compile("^[A-Z_-]*$")

    suspend fun addNewItem(request: CreateMedicationRequest, image: Mono<FilePart>): UUID {
        validateCode(request.body.code)
        var path: String? = null

        image.awaitSingleOrNull()?.let {
            val bytes = DataBufferUtils.join(it.content())
                .map { dataBuffer -> dataBuffer.asByteBuffer().array() }.awaitSingle()
            path = System.getProperty("user.home") + File.separator + it.filename()
            File(path!!).writeBytes(bytes)
        }

        val medication = medicationRepository.save(
            Medication(
                name = request.body.name,
                weight = request.body.weight,
                code = request.body.code,
                imagePath = path
            )
        )
        eventService.send(medication, EventState.CREATED)
        return medication.id!!
    }

    suspend fun getItemById(id: UUID): GetMedicationResponse {
        val entity = medicationRepository.findById(id) ?: throw BusinessException(
            message = "Medication with id = $id doesn't exist",
            errorCode = ErrorCode.NOT_FOUND
        )

        return GetMedicationResponse(
            id = entity.id!!,
            name = entity.name,
            weight = entity.weight,
            code = entity.code,
            image = if (entity.imagePath != null) {
                Base64.getEncoder().encodeToString(File(entity.imagePath).readBytes())
            } else {
                null
            }
        )
    }

    suspend fun findAll() = medicationRepository.findAll()

    suspend fun findAllById(ids: List<UUID>) =
        medicationRepository.findAllById(ids)

    private fun validateCode(code: String) {
        if (!pattern.matcher(code).find()) {
            throw BusinessException(
                message = "code must contains only uppercase letters and underscores or dashes",
                errorCode = ErrorCode.VALIDATION_CONSTRAINT
            )
        }
    }

}