package com.musala.droneservice.service

import com.musala.droneservice.dto.EventState
import com.musala.droneservice.dto.LoadDroneRequest
import com.musala.droneservice.dto.RegisterDroneRequest
import com.musala.droneservice.entity.Drone
import com.musala.droneservice.entity.DroneState
import com.musala.droneservice.exception.BusinessException
import com.musala.droneservice.exception.ErrorCode
import com.musala.droneservice.repository.DroneRepository
import com.musala.droneservice.util.charge
import com.musala.droneservice.util.doSomeWork
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.toList
import org.springframework.stereotype.Service
import java.util.UUID

@Service
class DroneService(
    private val droneRepository: DroneRepository,
    private val medicationService: MedicationService,
    private val commandService: CommandService,
    private val eventService: EventService
) {

    suspend fun createNewDrone(request: RegisterDroneRequest): UUID {
        droneRepository.findBySerialNumber(request.serialNumber)?.let {
            throw BusinessException(
                "Drone with the same serial number already registered",
                ErrorCode.VALIDATION_CONSTRAINT
            )
        }

        val drone = droneRepository.save(
            Drone(
                serialNumber = request.serialNumber,
                model = request.model,
                weightLimit = request.weightLimit,
                batteryCapacity = 100,
                state = DroneState.IDLE
            )
        )

        eventService.send(drone, EventState.CREATED)
        return drone.id!!
    }

    suspend fun setMedicationsToDrone(request: LoadDroneRequest) {
        val medicationsWithWeight = medicationService.findAllById(request.medicationIds)
            .map { Pair(it.weight, it) }
            .toList()
        val drone = findById(request.droneId)

        val isOverLimit = medicationsWithWeight.map { it.first }
            .reduce { sum, element -> sum + element } > drone.weightLimit

        if (isOverLimit) {
            throw BusinessException(
                message = "Medication items weight is greater than limit (${drone.weightLimit} gr)",
                errorCode = ErrorCode.VALIDATION_CONSTRAINT
            )
        } else {
            drone.apply {
                medicationsId = request.medicationIds
            }
            droneRepository.save(drone)
            eventService.send(drone, EventState.MEDICATIONS_ADDED)
        }
    }

    suspend fun getAll() = droneRepository.findAll()

    suspend fun getDroneById(id: UUID) = droneRepository.findById(id)

    suspend fun startDrone(id: UUID) =
        findById(id).let {
            checkDroneBeforeShipping(it)
            commandService.send(id)
        }

    suspend fun doWork(
        droneId: UUID, startState: DroneState, finishState: DroneState,
        reducePercentage: Int
    ) {
        droneRepository.findById(droneId)!!.let {
            droneRepository.save(
                it.copy(
                    state = startState
                )
            )
            eventService.send(it, startState)
            doSomeWork()
            droneRepository.save(
                it.copy(
                    state = finishState,
                    batteryCapacity = it.batteryCapacity - reducePercentage
                )
            )
            eventService.send(it, finishState)
        }
    }

    suspend fun startCharging() {
        droneRepository.findAllByStateAndBatteryCapacityBefore(DroneState.IDLE, MAX_BATTERY_PERCENTAGE)
            .map {
                charge()
                droneRepository.save(
                    it.apply {
                        batteryCapacity += 1
                    }
                )
            }
    }

    suspend fun removeMedicationsFromDrone(id: UUID) = findById(id).let {
        droneRepository.save(
            it.apply { medicationsId = null }
        )
        eventService.send(it, EventState.MEDICATIONS_REMOVED)
    }

    private suspend fun findById(id: UUID) = droneRepository.findById(id) ?: throw BusinessException(
        "Drone with id = $id doesn't exist",
        ErrorCode.NOT_FOUND
    )

    private fun checkDroneBeforeShipping(drone: Drone) {
        var errorMessage: String? = null
        if (drone.state != DroneState.IDLE) {
            errorMessage = "The drone with id = ${drone.id} must be in ${DroneState.IDLE}"
        } else if (drone.batteryCapacity < MIN_BATTERY_PERCENTAGE) {
            errorMessage = "The drone with id = ${drone.id} hasn't enough battery charge ${drone.batteryCapacity}"
        } else if (drone.medicationsId.isNullOrEmpty()) {
            errorMessage = "The drone with id = ${drone.id} must be loaded with medications"
        }

        if (errorMessage != null) {
            throw BusinessException(
                message = errorMessage,
                errorCode = ErrorCode.VALIDATION_CONSTRAINT
            )
        }
    }

    companion object {
        const val MIN_BATTERY_PERCENTAGE = 25
        const val MAX_BATTERY_PERCENTAGE = 100
    }

}